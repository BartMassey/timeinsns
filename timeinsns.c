#include <stdint.h>
#include <stdio.h>

extern uint64_t timeseq(void);

#define N 100000

int main() {
    uint64_t init = timeseq();
    init = timeseq();
    init = timeseq();
    init = timeseq();
    uint64_t max = init;
    uint64_t min = init;
    double mean = init;
    double mean2 = 0.0;
    for (uint64_t n = 1; n < N; n++) {
        double cycles;
        do {
            cycles = timeseq();
        } while (10 * cycles < 9 * mean || 9 * cycles > 10 * mean);
        if (cycles > max)
            max = cycles;
        if (cycles < min)
            min = cycles;
        double new_mean = mean + (cycles - mean) / n;
        mean2 += (cycles - mean) * (cycles - new_mean);
        mean = new_mean;
    }
    double var = mean2 / N;
    printf("%lu max  %lu min  mean %g  variance %g\n",
           max, min, mean, var);
    return 0;
}

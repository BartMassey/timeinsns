#include <stdint.h>
#include <stdlib.h>

/* Return a random number in the range [95..105]. */
uint64_t timeseq(void) {
    return 95 + rand() % 11;
}

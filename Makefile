CC = gcc
CFLAGS = -Wall -g

timeinsns: timeinsns.o timeseq.o
	$(CC) $(CFLAGS) -o timeinsns timeinsns.o timeseq.o

timeseq.o: timeseq.S
	as -gstabs+ -o timeseq.o timeseq.S

clean:
	-rm -f *.o timeinsns

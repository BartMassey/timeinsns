#include <stdint.h>
#include <stdlib.h>

volatile uint64_t d = 0x123456789abcdef0L;
volatile uint32_t d1 = 11;
volatile uint32_t d2 = 13;

/* Return a random number in the range [95..105]. */
uint64_t timeseq(void) {
    uint32_t q1 = d / d1;
    uint32_t q2 = (uint64_t) q1 / d2;
    return (uint64_t) q2;
}
